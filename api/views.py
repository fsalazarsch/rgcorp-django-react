import requests, json
from django.views.generic import View

from django.http import HttpRequest, JsonResponse, HttpResponse
from django.utils.datastructures import MultiValueDictKeyError

from .TableParser import TableParser
from .models import Scraper

url ="https://www.coinmarketcap.com"

class ScraperAPI(View):
    def get(self, *args, **kwargs):

        scrapers = list(Scraper.objects.values("id", "created_at", "currency", "frequency", "value", "updated_at"))
        return JsonResponse({"scrapers" : scrapers}, safe=False)

    def post(self, request, *args, **kwargs):

        r = requests.get(url)
        rpta = r.text
        
        try:
            currency = request.POST['currency']
        except MultiValueDictKeyError:
            return JsonResponse( {"error": "Debe ingresar una criptomoneda"})

        try:
            frequency = request.POST["frequency"]
        except MultiValueDictKeyError:
            return JsonResponse( {"error": "Debe ingresar una frecuencia"})

        t = TableParser(currency, frequency)
        return t
        pass

    def put(self, request, *args, **kwargs):

        body = {}
        bodystr = str(request.body.decode('utf-8')).split("&")
        for item in bodystr:
            aux = item.split("=")
            body[ aux[0] ] = aux[1]


        try:
            id = body['id']
        except KeyError:
            return JsonResponse( {"error": "Debe ingresar un ID"})

        try:
            frequency = body["frequency"]
            if int (frequency) > 30 or  int (frequency) < 0:
                return JsonResponse( {"error": "Debe ingresar una frecuencia entre 1 y 30"})            	
        except KeyError:
            return JsonResponse( {"error": "Debe ingresar una frecuencia"})

        try:
            scraper = Scraper.objects.get(pk=id)
            scraper.frequency = frequency
            scraper.save()
        except scraper.DoesNotExist:
            return JsonResponse( {"error": "Debe ingresar una id de criptomoneda existente"})

        return JsonResponse( {"msg": "Se ha modificado la frecuencia correctamente"})


    def delete(self, request, *args, **kwargs):

        body = {}
        bodystr = str(request.body.decode('utf-8')).split("&")
        for item in bodystr:
            aux = item.split("=")
            body[ aux[0] ] = aux[1]

        try:
            id = body['id']
        except KeyError:
            return JsonResponse( {"error": "Debe ingresar un ID"})
        
        try:
            Scraper.objects.get(pk=id).delete()
        except Scraper.DoesNotExist:
        	return JsonResponse( {"error": "Debe ingresar una id de criptomoneda existente"})
        
        return JsonResponse( {"msg": "Se ha eliminado la criptomoneda correctamente"})
        pass
