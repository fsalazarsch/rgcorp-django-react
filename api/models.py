from django.db import models

class Scraper(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    currency = models.CharField(max_length=25, unique=True)
    frequency = models.SmallIntegerField(default=1)
    value = models.FloatField(default=1)    
    updated_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.currency