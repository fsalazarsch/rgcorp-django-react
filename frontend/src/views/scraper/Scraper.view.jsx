import React from 'react'
import CardContainer from '../../components/CardContainer'
import Table from '../../components/TableContainer'
import Footer from '../../components/FooterContainer'
import Formulario from '../../components/FormularioContainer'


const ScraperView = () => {
  return (
    // Edit this as you like!
    <CardContainer height='30vh' width='50%' margin='2rem auto' padding='2rem' justifyContent='center' alignItems='center'>


      <Formulario
      nombre="Actualizar"
      classname="table"
      classnamebutton="primary"
      ></Formulario>
      <Table>
      </Table>
      <Footer>
      </Footer>
    </CardContainer>
  )
}

export default ScraperView