import React from 'react'


class Footer extends React.Component{

  constructor(props) {
    super(props);
    this.state = { promedio: ""};
  }

      async componentDidMount() {
           await this.getPromedio()

    }


    getPromedio = async () => {
      let res = await fetch('http://localhost:8000/api/promedio')
      let promedio = await res.json()
      promedio = parseFloat(promedio.promedio.frequency__avg).toFixed(2)
      //console.log(promedio)
      this.setState({
            promedio
      })
      
    }


    render() {
    return (

        <div>El promedio de las frecuencias es: {this.state.promedio}</div>

        );
 
}

}
export default Footer

