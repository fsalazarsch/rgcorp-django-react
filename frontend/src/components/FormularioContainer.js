import React from 'react'
import "./styles/button.css"


class Formulario extends React.Component{

  constructor(props) {
    super(props);
    this.state = { frequency : 0, currency: "", data: []};
    this.handleCrear = this.handleCrear.bind(this);
    this.handlefrecuencia = this.handlefrecuencia.bind(this);
    this.handlemoneda = this.handlemoneda.bind(this);
    this.handleActualizar = this.handleActualizar.bind(this);

  }
    
     componentDidMount() {
         fetch('http://localhost:8000/api/scrapers')
        .then(response => response.json())
        .then(data => this.setState({ data: data.scrapers }));

    }

   


  handlefrecuencia(e){
    this.setState({ frequency: e.target.value });

  }

  handlemoneda(e){
    this.setState({ currency: e.target.value });

  }
  handleCrear = async () => {
  	console.log(this.state)
  	let payload = {
  		method: 'POST',
  		headers: {
  			'Content-Type' : 'application/x-www-form-urlencoded'
  		},
  		body : 'currency='+this.state.currency+'&frequency='+this.state.frequency

  	}
  	let res = await fetch('http://localhost:8000/api/scrapers', payload);
  	let json = await res.json()

  	console.log(json)
  	if (json.msg)
	  	alert(json.msg)
	else
		alert("La criptomoneda "+this.state.currency+" fue creada correctamente")
  	}

  handleActualizar=  async() => {
  	console.log(this.state)

		let item = this.state.data.find(el => el.currency.toLowerCase() === this.state.currency.toLowerCase());
		console.log(item["id"]);

  	let payload2 = {
  		method: 'PUT',
  		headers: {
  			'Content-Type' : 'application/x-www-form-urlencoded'
  		},
  		body : 'id='+item["id"]+'&frequency='+this.state.frequency

  	}
  	let res2 =  await fetch('http://localhost:8000/api/scrapers', payload2);
  	let json2 =  await res2.json()
  	console.log(json2)

  	if (json2.msg)
	  	alert(json2.msg)  	
    
  	} 	
  

  render() {
    return (

    	<div className="table">
        <label>Frecuencia (segundos)</label>
        <input type="number" onChange={this.handlefrecuencia}/>
        
        <label>Moneda</label>
        <input  onChange={this.handlemoneda }/>

        <button className={this.props.classnamebutton} onClick={this.handleCrear}>Crear</button>
        <button className={this.props.classnamebutton} onClick={this.handleActualizar}>Actualizar</button>
        </div>
  )}
}

export default Formulario

