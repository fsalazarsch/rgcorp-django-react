import React from 'react'

let meses = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
 "Noviembre", "Diciembre" ];

class Table extends React.Component{

  constructor(props) {
    super(props);
    this.state = { data : [] };
  }

      async componentDidMount() {
            await this.getCriptomonedas()

    }

    getCriptomonedas = async () => {
      let res = await fetch('http://localhost:8000/api/scrapers')
      let data = await res.json()
      data = data.scrapers

      this.setState({
            data
      })

    }


    render() {
    return (
        <table border ="1">
              <thead>
    <tr>
      <th>ID</th>
      <th>Moneda</th>
      <th>Último valor leido</th>
      <th>Frecuencia</th>
      <th>Fecha de Creación</th>
    </tr>
  </thead><tbody>
            {this.state.data.map((item) =>{
                        item.created_at = item.created_at.substr(11, 5)+" "+
                        meses[(new Date(item.created_at).getMonth())]+" "+new Date(item.created_at).getDate() 
                        return(
                                    <tr>
                                    <td>{item.id}</td>
                                    <td>{item.currency}</td>
                                    <td>$ {item.value}</td>
                                    <td>{item.frequency}</td>
                                    <td>{item.created_at}</td>
                                    </tr>
                              
                              )
                  })
            }
           
        </tbody></table>
        );
 
}

}
export default Table

